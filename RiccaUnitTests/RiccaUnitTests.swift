//
//  RiccaUnitTests.swift
//  RiccaUnitTests
//
//  Created by Andrei Ialanzhi on 27.07.2018.
//  Copyright © 2018 Andrei Ialanzhi. All rights reserved.
//

import XCTest
@testable import ricca

class RiccaUnitTests: XCTestCase {
    let controller = CalculatorController()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCountDays() {
        let tests = [
            ["1 01 2018", "3 01 2018", "360", "3"],
            ["1 01 2018", "3 01 2018", "365", "3"],
            
            ["1 05 2018", "31 05 2018", "360", "30"],
            ["1 05 2018", "31 05 2018", "365", "31"],
            
            ["1 05 2018", "30 06 2018", "360", "60"],
            ["1 05 2018", "30 06 2018", "365", "61"],
            
            ["1 02 2016", "29 02 2016", "360", "30"],
            ["1 02 2016", "29 02 2016", "365", "29"],
            
            ["1 02 2016", "28 02 2016", "360", "30"],
            ["1 02 2016", "28 02 2016", "365", "28"],
            
            ["1 10 2015", "12 08 2017", "360", "672"],
            ["1 10 2015", "12 08 2017", "365", "682"],
        ]
        
        for test in tests {
            let result = controller.countDays(fromDate: test[0].toDate, toDate: test[1].toDate, daysInMonth: Int(test[2])!)
            
            XCTAssertEqual(Int(test[3])!, result)
        }
    }
    
    func testPerformanceExample() {
        self.measure {
            testCountDays()
        }
    }
}
