//
//  ViewController.swift
//  nitti
//
//  Created by Andrew Yalanzhi on 28.02.15.
//  Copyright (c) 2015 Andrei Ialanzhi. All rights reserved.
//

import UIKit

class PoshlinaController: UIViewController, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var scrollDisplay: UIScrollView!
    var pageControl: UIPageControl!

    let courts = [["Арбитражный суд", ""], ["Арбитражный суд", "Приказ"], ["Суд общей юрисдикции", ""], ["Суд общей юрисдикции", "Приказ"], ["Права потребителей", ""]]
    var dutyLabels:[UILabel] = []
    var costsLabels:[UILabel] = []
    var arrows:[UIImageView] = []
    
    var copyLabel: UILabel!
    var timer = Timer()
    
    let cButtons = ["7", "8", "9", "4", "5", "6", "1", "2", "3", ",", "0", "backspace"]
    
    var firstCostsDictionary:[String:Bool] = ["isTypping": false, "isCommaAvailable": true, "firstDecimal": false, "secondDecimal": false]
    var secondCostsDictionary:[String:Bool] = ["isTypping": false, "isCommaAvailable": true, "firstDecimal": false, "secondDecimal": false]

    var surcharge: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scrollDisplay = UIScrollView()
        scrollDisplay.translatesAutoresizingMaskIntoConstraints = false
        scrollDisplay.isPagingEnabled = true
        scrollDisplay.showsHorizontalScrollIndicator = false
        scrollDisplay.delegate = self
        scrollDisplay.backgroundColor = UIColor.clear
        view.addSubview(scrollDisplay)
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: (self.view.bounds.width / 3), height: ((self.view.bounds.height * 0.382) / 4))

        let keypadDisplay: UICollectionView! = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        keypadDisplay.backgroundColor = UIColor.clear
        keypadDisplay.dataSource = self
        keypadDisplay.delegate = self
        keypadDisplay.register(KeypadCell.self, forCellWithReuseIdentifier: "KeypadCell")
        keypadDisplay.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "backspaceCell")
        keypadDisplay.translatesAutoresizingMaskIntoConstraints = false
        keypadDisplay.delaysContentTouches = false
        view.addSubview(keypadDisplay)

        var displays:[UIStackView] = []

        for court in courts {
            
            let stackView = UIStackView()
            stackView.distribution = .fill
            stackView.axis = .vertical
            stackView.spacing = 4
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.layoutMargins = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 19)
            stackView.isLayoutMarginsRelativeArrangement = true
            
            scrollDisplay.addSubview(stackView)
            
            let courtStack = UIView()
            
            let courtLabel = UILabel()
            courtLabel.numberOfLines = 1
            courtLabel.text = court[0]
            courtLabel.textColor = UIColor.darkGray
            courtLabel.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
            courtLabel.translatesAutoresizingMaskIntoConstraints = false
            courtStack.addSubview(courtLabel)
            
            let courtSubLabel = UILabel()
            courtSubLabel.numberOfLines = 1
            courtSubLabel.text = court[1]
            courtSubLabel.textColor = UIColor.lightGray
            courtSubLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
            courtSubLabel.translatesAutoresizingMaskIntoConstraints = false
            courtStack.addSubview(courtSubLabel)
            
            courtStack.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[courtLabel]-6-[courtSubLabel]", options: .alignAllTop, metrics: nil, views: ["courtLabel": courtLabel, "courtSubLabel": courtSubLabel]))
            courtStack.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[courtLabel]|", options: [], metrics: nil, views: ["courtLabel": courtLabel, "courtSubLabel": courtSubLabel]))
            
            let dutyStack = UIView()
            
            let dutyLabel = UILabel()
            dutyLabel.numberOfLines = 1
            dutyLabel.textColor = blueTheme["main"]!
            dutyLabel.font = UIFont.monospacedDigitSystemFont(ofSize: UIFont.systemFontSize * 3.25, weight: UIFont.Weight.regular)
            dutyLabel.text = "0,00"
            dutyLabel.translatesAutoresizingMaskIntoConstraints = false
            dutyStack.addSubview(dutyLabel)
            
            dutyLabels.append(dutyLabel)
            
            let arrowsign = UIImageView(image: UIImage(named: "arrow")!)
            arrowsign.alpha = 0
            arrowsign.translatesAutoresizingMaskIntoConstraints = false
            
            dutyStack.addSubview(arrowsign)
            
            dutyStack.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dutyLabel][arrowsign(20)]|", options: .alignAllCenterY, metrics: nil, views: ["arrowsign": arrowsign, "dutyLabel": dutyLabel]))
            dutyStack.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[arrowsign(12)]", options: [], metrics: nil, views: ["arrowsign": arrowsign, "dutyLabel": dutyLabel]))
            dutyStack.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dutyLabel]|", options: [], metrics: nil, views: ["arrowsign": arrowsign, "dutyLabel": dutyLabel]))
            
            self.arrows.append(arrowsign)
            
            stackView.addArrangedSubview(courtStack)
            stackView.addArrangedSubview(dutyStack)
            
            NSLayoutConstraint.activate([
                stackView.centerYAnchor.constraint(equalTo: scrollDisplay.centerYAnchor, constant: 12)
                ])
            
            displays.append(stackView)
        }
        
        let costsDisplay = PoshlinaDisplay()
        costsDisplay.translatesAutoresizingMaskIntoConstraints = false

        for i in 0..<2 {
            let dutyLabel = UILabel()
            dutyLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 22, weight: UIFont.Weight.light)
            dutyLabel.text = "0,00"
            dutyLabel.textColor = UIColor.darkGray
            if i == 1 {
                dutyLabel.alpha = 0.0
            }
            dutyLabel.translatesAutoresizingMaskIntoConstraints = false
            costsDisplay.addSubview(dutyLabel)
            
            let costsDictionary = ["dutyLabel": dutyLabel]
            
            costsDisplay.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-12-[dutyLabel]-12-|", options: [], metrics: nil, views: costsDictionary))

             costsDisplay.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-24-[dutyLabel]-54-|", options: [], metrics: nil, views: costsDictionary))
            
            costsLabels.append(dutyLabel)
        }

        self.view.addSubview(costsDisplay)
        
        pageControl = UIPageControl()
        pageControl.numberOfPages = displays.count
        pageControl.currentPageIndicatorTintColor = blueTheme["main"]!
        pageControl.pageIndicatorTintColor = blueTheme["lightGrey"]!
        pageControl.addTarget(self, action: #selector(self.whf), for: .valueChanged)
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageControl)
        
        view.bringSubview(toFront: pageControl)
        
        let viewsDictionary = ["scrollDisplay": scrollDisplay, "keypadDisplay": keypadDisplay, "pageControl": pageControl, "arbitralCourtDisplay": displays[0], "arbitralCourtWritDisplay": displays[1], "sumpremeCourtDisplay": displays[2], "writDisplay": displays[3], "consumerDisplay": displays[4], "costsDisplay": costsDisplay] as [String : Any]
        
        let courtDisplaysHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|[arbitralCourtDisplay(==keypadDisplay)][arbitralCourtWritDisplay(==arbitralCourtDisplay)][sumpremeCourtDisplay(==arbitralCourtDisplay)][writDisplay(==arbitralCourtDisplay)][consumerDisplay(==arbitralCourtDisplay)]|", options: [], metrics: nil, views: viewsDictionary)
        
        let pageDisplayVertical = NSLayoutConstraint.constraints(withVisualFormat: "V:[pageControl(==10)]-[costsDisplay]", options: [], metrics: nil, views: viewsDictionary)
        
        let pageControlHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|[pageControl(==arbitralCourtDisplay)]|", options: [], metrics: nil, views: viewsDictionary)
        
        let scrollDisplayConstraintsHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|[scrollDisplay(==keypadDisplay)]|", options: [], metrics: nil, views: viewsDictionary)
        
        let keypadDisplayConstrainsHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|[keypadDisplay]|", options: [], metrics: nil, views: viewsDictionary)
        
        let costsDisplayConstrainsHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|[costsDisplay]|", options: [], metrics: nil, views: viewsDictionary)
        
        let kC = NSLayoutConstraint(item: keypadDisplay, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.height, multiplier: 0.382, constant: 1)
        
        let viewsArrangement = NSLayoutConstraint.constraints(withVisualFormat: "V:|[scrollDisplay][costsDisplay][keypadDisplay]-54-|", options: [], metrics: nil, views: viewsDictionary)
        
        view.addConstraint(kC)
        view.addConstraints(scrollDisplayConstraintsHorizontal)
        view.addConstraints(pageControlHorizontal)
        view.addConstraints(pageDisplayVertical)
        view.addConstraints(costsDisplayConstrainsHorizontal)
        view.addConstraints(keypadDisplayConstrainsHorizontal)
        view.addConstraints(viewsArrangement)
        view.addConstraints(courtDisplaysHorizontal)
        
        let copyTap = UITapGestureRecognizer(target: self, action: #selector(copyDutyLabel))
        copyTap.numberOfTapsRequired = 2
        scrollDisplay.addGestureRecognizer(copyTap)
        
        let fDaCentsGesture = UITapGestureRecognizer(target: self, action: #selector(fDaCents))
        fDaCentsGesture.numberOfTapsRequired = 1
        fDaCentsGesture.require(toFail: copyTap)
        scrollDisplay.addGestureRecognizer(fDaCentsGesture)
        
        let pasteTap = UITapGestureRecognizer(target: self, action: #selector(pasteCostsLabel))
        pasteTap.numberOfTapsRequired = 2
        costsDisplay.addGestureRecognizer(pasteTap)
        
        let swipeToDelete = UISwipeGestureRecognizer(target: self, action: #selector(clearLogic))
        swipeToDelete.direction = .left
        costsDisplay.addGestureRecognizer(swipeToDelete)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scrollToPage(UserDefaults.standard.integer(forKey: "prefferedDisplay"), animated: false)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if cButtons[(indexPath as NSIndexPath).row] == "backspace" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "backspaceCell", for: indexPath)
            let image = UIImageView(image: UIImage(named: "backspace"))
            image.translatesAutoresizingMaskIntoConstraints = false
            
            cell.addSubview(image)
            
            cell.addConstraint(NSLayoutConstraint(item: image, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: cell, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
            
            cell.addConstraint(NSLayoutConstraint(item: image, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: cell, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            
            let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(CalculatorController.clearLongPressed(_:)))
            longPressGesture.minimumPressDuration = 0.25
            cell.addGestureRecognizer(longPressGesture)
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KeypadCell", for: indexPath) as! KeypadCell
            cell.textLabel.text = self.cButtons[(indexPath as NSIndexPath).row]
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath)! as? KeypadCell {
            self.numericAndCommaPressed(cell.textLabel.text!)
        } else {
            self.clearLogic()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath)! as? KeypadCell {
            cell.textLabel.textColor = blueTheme["main"]!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath)! as? KeypadCell {
            cell.textLabel.textColor = UIColor.darkGray
        }
    }
    
    @objc func fDaCents() {
        UserDefaults.standard.set((UserDefaults.standard.bool(forKey: "prefferedStyle") == true) ? false : true, forKey: "prefferedStyle")
        countAndDoTheMath()
    }
    
    @objc func copyDutyLabel() {
        UIPasteboard.general.string = dutyLabels[self.pageControl.currentPage].text!
        
        self.copyLabel = UILabel(frame: CGRect(x: 0, y: 20, width: view.bounds.size.width, height: 20))
        self.copyLabel.text = "Скопировано"
        self.copyLabel.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize, weight: UIFont.Weight.regular)
        self.copyLabel.textAlignment = .center
        self.copyLabel.textColor = blueTheme["main"]!
        self.copyLabel.alpha = 0.0
        view.addSubview(self.copyLabel)
        view.bringSubview(toFront: self.copyLabel)
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.copyLabel.alpha = 1.0
        }, completion: nil)
        
        UIView.animate(withDuration: 1.0, delay: 1.5, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.copyLabel.alpha = 0.0
        }, completion: {_ in self.copyLabel.removeFromSuperview()})
    }
    
    @objc func pasteCostsLabel() {
        let cleanString = (UIPasteboard.general.string == nil) ? "0,00" : UIPasteboard.general.string!.stripAllDigits()

        var costDictionary:[String: Bool] = (surcharge) ? secondCostsDictionary : firstCostsDictionary
        
        if !cleanString.isEmpty {
            costDictionary = ["isTypping": true, "isCommaAvailable": false, "firstDecimal": true, "secondDecimal": true]
        }
        
        self.setSurchargeDictionary(costDictionary)

        let costLabel = (surcharge) ? costsLabels[1] : costsLabels[0]
        costLabel.attributedText = StringHelpers.shared.changeColor(string: MathController().numberFormatter.string(from: NSNumber(floatLiteral: (cleanString as NSString).doubleValue))!, withDictionary: costDictionary)
        countAndDoTheMath()
    }
    
    @objc func clearLongPressed(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            self.timer = Timer.scheduledTimer(timeInterval: 0.35, target: self, selector: #selector(clearLogic), userInfo: nil, repeats: true)
        case .ended:
            self.timer.invalidate()
        default:
            ()
        }
    }
    
    func setSurchargeDictionary(_ costDictionary:[String: Bool]) {
        if surcharge {
            self.secondCostsDictionary = costDictionary
        } else {
            self.firstCostsDictionary = costDictionary
        }
    }
    
    @objc func clearLogic() {
        let costLabel = (surcharge) ? costsLabels[1] : costsLabels[0]
        let existingCharacter = costLabel.text!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ",", with: ".")

        var clearedCharactres:String! = "0,00"
        
        var costDictionary:[String: Bool] = (surcharge) ? secondCostsDictionary : firstCostsDictionary
        
        if costDictionary["isTypping"] == true {
            if costDictionary["isCommaAvailable"] == true {
                let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 3)
                var result = String(existingCharacter[..<index])
                result.removeLast()
                clearedCharactres = result
            } else {
                if costDictionary["firstDecimal"] == false && costDictionary["secondDecimal"] == false {
                    costDictionary["isCommaAvailable"] = true
                    let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 3)
                    clearedCharactres = String(existingCharacter[..<index])
                }
                if costDictionary["firstDecimal"] == true && costDictionary["secondDecimal"] == false {
                    let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 2)
                    clearedCharactres = String(existingCharacter[..<index])

                    costDictionary["firstDecimal"] = false
                } else if costDictionary["firstDecimal"] == true && costDictionary["secondDecimal"] == true {
                    let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 1)
                    clearedCharactres = String(existingCharacter[..<index])

                    costDictionary["secondDecimal"] = false
                }
            }
        }

        if (clearedCharactres as NSString).doubleValue.isZero && costDictionary["isCommaAvailable"] == true {
            costDictionary["isTypping"] = false
        }
        
        self.setSurchargeDictionary(costDictionary)
        
        costLabel.attributedText = StringHelpers.shared.changeColor(string: MathController().numberFormatter.string(from: NSNumber(floatLiteral: (clearedCharactres as NSString).doubleValue))!, withDictionary: costDictionary)
        countAndDoTheMath()
    }
    
    func numericAndCommaPressed(_ typpedCharacter: String) {
        let costLabel = (surcharge) ? costsLabels[1] : costsLabels[0]
        let existingCharacter = costLabel.text!
        let costDictionary:[String: Bool] = (surcharge) ? secondCostsDictionary : firstCostsDictionary
        let characterCounter = existingCharacter.split(separator: ",")[0].count

        if characterCounter < 15 {
            if costDictionary["secondDecimal"]! == false {
                showCosts(typpedCharacter, existingCharacter: existingCharacter, costLabel: costLabel)
            }
        } else if characterCounter == 15 {
            if typpedCharacter == "," {
                showCosts(typpedCharacter, existingCharacter: existingCharacter, costLabel: costLabel)
            } else {
                if costDictionary["isCommaAvailable"]! == false {
                    if costDictionary["secondDecimal"] == false {
                        showCosts(typpedCharacter, existingCharacter: existingCharacter, costLabel: costLabel)
                    }
                }
            }
        }
    }
    
    func showCosts(_ typpedCharacter:String, existingCharacter:String, costLabel:UILabel) {
        let existingCharacter = existingCharacter.replacingOccurrences(of: " ", with: "")
        var combinedCharacters = String(stringInterpolationSegment: MathController().numberFormatter.number(from: existingCharacter)!) + typpedCharacter
        var doubleResult = MathController().numberFormatter.string(from: NSNumber(floatLiteral: (combinedCharacters as NSString).doubleValue))!
        var costDictionary:[String: Bool] = (surcharge) ? secondCostsDictionary : firstCostsDictionary

        if costDictionary["isTypping"] == true {
            if typpedCharacter == "," {
                if costDictionary["isCommaAvailable"] == true {
                    costDictionary["isCommaAvailable"] = false
                }
            } else {
                if costDictionary["isCommaAvailable"] == false {
                    if costDictionary["firstDecimal"] == false && costDictionary["secondDecimal"] == false {
                        let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 3)

                        combinedCharacters = String(existingCharacter[..<index]) + "." + typpedCharacter
                        costDictionary["firstDecimal"] = true
                        doubleResult = MathController().numberFormatter.string(from: NSNumber(floatLiteral: (combinedCharacters as NSString).doubleValue))!
                    } else if costDictionary["firstDecimal"] == true && costDictionary["secondDecimal"] == false {
                        let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 1)
                        combinedCharacters = String(existingCharacter[..<index]) + typpedCharacter
                        combinedCharacters = combinedCharacters.replacingOccurrences(of: ",", with: ".")
                        costDictionary["secondDecimal"] = true
                        doubleResult = MathController().numberFormatter.string(from: NSNumber(floatLiteral: (combinedCharacters as NSString).doubleValue))!
                    }
                }
            }
        } else {
            costDictionary["isTypping"] = true
            if typpedCharacter == "," {
                costDictionary["isCommaAvailable"] = false
            }
        }
        
        self.setSurchargeDictionary(costDictionary)
        
        costLabel.attributedText = StringHelpers.shared.changeColor(string: doubleResult, withDictionary: costDictionary)
        countAndDoTheMath()
    }

    func countAndDoTheMath() {
        let duties: [String]!
        let firstDuty:String = costsLabels[0].text!

        if surcharge {
            let secondDuty:String = costsLabels[1].text!
            
            duties = MathController().countSurchargeDuties(firstDuty, secondCostsOfAction: secondDuty)
        } else {
            duties = MathController().countDuties(firstDuty)
        }

        for display in 0..<duties.count {
            putDutyIntoLabel(display, duty: duties[display])
        }
    }
    
    func putDutyIntoLabel(_ labelNumber: Int, duty: String) {
        dutyLabels[labelNumber].text = duty.replacingOccurrences(of: "-", with: "")

        if self.surcharge {
            showArrow(labelNumber, countedDuty: duty.toDouble)
        }
    }
    
    func showArrow(_ displayNumber: Int, countedDuty: Double) {
        if countedDuty < 0 {
            self.arrows[displayNumber].alpha = 1.0
            self.arrows[displayNumber].transform = CGAffineTransform(rotationAngle: CGFloat((180.0 * Double.pi) / 180.0))
        } else if countedDuty > 0 {
            self.arrows[displayNumber].alpha = 1.0
            self.arrows[displayNumber].transform = CGAffineTransform(rotationAngle: CGFloat((180.0 * Double.pi) * 180.0))
        } else {
            self.arrows[displayNumber].alpha = 0.0
        }
    }

    @objc func stateChanged(_ sender: UIButton) {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: [], animations: {
            sender.transform = self.surcharge ? CGAffineTransform(rotationAngle: CGFloat(0)) : CGAffineTransform(rotationAngle: CGFloat(Double.pi / 4))
            self.costsLabels[0].alpha = (self.surcharge) ? 1.0 : 0.0
            self.costsLabels[1].alpha = (self.surcharge) ? 0.0 : 1.0
            self.surcharge = (self.surcharge) ? false : true
            if !self.surcharge {
                for i in 0..<self.arrows.count { self.arrows[i].alpha = 0.0 }
            }
        }, completion: nil)
        self.countAndDoTheMath()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage:Int = NSInteger(scrollDisplay.contentOffset.x / scrollDisplay.frame.size.width)
        self.pageControl.currentPage = currentPage
        save(currentPage)
    }
    
    func scrollToPage(_ currentPage:Int, animated: Bool) {
        let goToPage: CGRect = scrollDisplay!.frame
        
        self.scrollDisplay!.setContentOffset(CGPoint(x: (goToPage.size.width * CGFloat(currentPage)), y: 0), animated: animated)
        self.pageControl.currentPage = currentPage
        save(currentPage)
    }
    
    func save(_ currentPage: Int) {
        UserDefaults.standard.set(currentPage, forKey: "prefferedDisplay")
    }
    
    @objc func whf() {
        scrollToPage(self.pageControl.currentPage, animated: true)
    }
}
