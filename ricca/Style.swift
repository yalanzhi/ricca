//
//  Style.swift
//  Karma395
//
//  Copyright (c) 2015 Andrei Ialanzhi. All rights reserved.
//

import UIKit

let blueTheme:[String:UIColor] = [
    "main": UIColor(
        red:   0.0/255.0,
        green: 140.0/255.0,
        blue:  190.0/255.0,
        alpha: 1.0
    ),
    
    "lightGrey":  UIColor(white: 250.0/255.0, alpha: 1.0),
    "background": UIColor(white: 255.0/255.0, alpha: 1.0)
]
