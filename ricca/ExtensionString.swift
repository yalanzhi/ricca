
//
//  ExtensionString.swift
//  nitti
//
//  Created by Andrei Ialanzhi on 19.08.16.
//  Copyright © 2016 Andrei Ialanzhi. All rights reserved.
//
import UIKit

extension String {

    func stripAllDigits() -> String {

        let pattern = "[^0-9.,]|^((\\.|\\,){0,})|((\\.|\\,){0,})$"
        let regularExpression = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)

        let result = regularExpression!.stringByReplacingMatches(in: self, options: [], range: NSMakeRange(0, self.count), withTemplate: "").replacingOccurrences(of: ",", with: ".")
        
        return result
    }
}
