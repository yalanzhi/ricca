//
//  ViewController.swift
//  Karma395
//
//  Copyright (c) 2015 Andrei Ialanzhi. All rights reserved.
//

import UIKit

class CalculatorController: UIViewController, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    var scrollDisplay: UIScrollView!
    var pageControl: UIPageControl!

    let ratesOptions:[String] = ["Проценты по 395 ГК РФ", "Проценты по 317.1 ГК РФ"]

    var keypad: UIView!
    var cDisplay: CostsDisplay!
    var daysAlertView: DaysAlertView!
    var regionAlertView: RegionAlertView!

    var cTitleLabel: UILabel!
    var digitalLabel: UILabel!
    var copyLabel: UILabel!

    let cButtons = ["VAT", "Days", "CBR"]
    let cButtons2 = ["7", "8", "9", "4", "5", "6", "1", "2", "3", ",", "0", "backspace"]
    var buttons:[UIButton] = []

    var howToButtons: [UIButton] = []
    var calendarBtn: UIButton!
    var regionPicker: UIButton!
    let availableRegions = ["ЦФО", "СЗФО", "ЮФО", "СКФО", "ПФО", "УФО", "СФО", "ДФО", "КФО"]

    var stages:[RateStage] =  [RateStage]()
    var stages317:[RateStage] =  [RateStage]()

    var costDictionary:[String:Bool] = ["isTypping": false, "isCommaAvailable": true, "firstDecimal": false, "secondDecimal": false]

    var timer = Timer()
    var rateLabels:[UILabel]! = []

    var dutyCurrencyFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.roundingMode = .halfDown
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.groupingSeparator = " "
        formatter.decimalSeparator = ","
        return formatter
    }

    var costsCurrencyFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.roundingMode = .down
        formatter.maximumIntegerDigits = 10
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.groupingSeparator = " "
        formatter.decimalSeparator = ","
        return formatter
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUserPrefferences()

        self.copyLabel = UILabel()
        self.copyLabel.text = "Скопировано"
        self.copyLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        self.copyLabel.textAlignment = .left
        self.copyLabel.textColor = blueTheme["main"]!
        self.copyLabel.alpha = 0.0
        self.copyLabel.translatesAutoresizingMaskIntoConstraints = false

        self.calendarBtn = UIButton()
        self.calendarBtn.setTitleColor(blueTheme["main"]!, for: .normal)
        self.calendarBtn.setTitleColor(blueTheme["main"]!.withAlphaComponent(0.7), for: .highlighted)
        self.calendarBtn.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        let title = (UserDefaults.standard.integer(forKey: "usersCalendar") == 360) ? "360" : "365/366"
        self.calendarBtn.setTitle(title, for: .normal)
        self.calendarBtn.layer.borderWidth = 1.25
        self.calendarBtn.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.1).cgColor
        self.calendarBtn.layer.cornerRadius = 5
        self.calendarBtn.addTarget(self, action: #selector(CalculatorController.changeCalendar(_:)), for: .touchUpInside)
        self.calendarBtn.translatesAutoresizingMaskIntoConstraints = false

        self.regionPicker = UIButton()
        self.regionPicker.setTitleColor(blueTheme["main"]!, for: .normal)
        self.regionPicker.setTitleColor(blueTheme["main"]!.withAlphaComponent(0.7), for: .highlighted)
        self.regionPicker.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        self.regionPicker.setTitle(self.availableRegions[UserDefaults.standard.integer(forKey: "usersRegion")], for: .normal)
        self.regionPicker.layer.borderWidth = 1
        self.regionPicker.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.1).cgColor
        self.regionPicker.layer.cornerRadius = 5
        self.regionPicker.addTarget(self, action: #selector(CalculatorController.regionPickerPressed(_:)), for: .touchUpInside)
        self.regionPicker.translatesAutoresizingMaskIntoConstraints = false

        let topBar = UIView()
        topBar.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(topBar)

        topBar.addSubview(self.copyLabel)
        topBar.addSubview(self.calendarBtn)
        topBar.addSubview(self.regionPicker)

        let topBarViews: [String: Any] = ["copyLabel": self.copyLabel, "regionPicker": self.regionPicker, "calendarBtn": self.calendarBtn]

        topBar.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[copyLabel]-[regionPicker(72)]-[calendarBtn(==regionPicker)]-16-|", options: .alignAllLastBaseline, metrics: nil, views: topBarViews))
        topBar.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-16-[calendarBtn]|", options: .alignAllLastBaseline, metrics: nil, views: topBarViews))

        scrollDisplay = UIScrollView()
        scrollDisplay.translatesAutoresizingMaskIntoConstraints = false
        scrollDisplay.isPagingEnabled = true
        scrollDisplay.showsHorizontalScrollIndicator = false
        scrollDisplay.delegate = self
        scrollDisplay.backgroundColor = .clear

        var displays:[UIStackView] = []

        for ratesOption in ratesOptions {

            let stackView = UIStackView()
            stackView.distribution = .fill
            stackView.axis = .vertical
            stackView.spacing = 4
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.layoutMargins = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 19)
            stackView.isLayoutMarginsRelativeArrangement = true

            scrollDisplay.addSubview(stackView)

            let courtStack = UIStackView()
            courtStack.distribution = .fillProportionally
            courtStack.axis = .horizontal
            courtStack.translatesAutoresizingMaskIntoConstraints = false
            courtStack.isLayoutMarginsRelativeArrangement = false

            let courtLabel = UILabel()
            courtLabel.numberOfLines = 1
            courtLabel.text = ratesOption
            courtLabel.textColor = UIColor.darkGray
            courtLabel.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
            courtLabel.translatesAutoresizingMaskIntoConstraints = false
            courtStack.addArrangedSubview(courtLabel)

            let howToButton = UIButton()
            howToButton.setTitle("Подробнее", for: .normal)
            howToButton.setTitleColor(blueTheme["main"]!, for: .normal)
            howToButton.setTitleColor(blueTheme["main"]!.withAlphaComponent(0.7), for: .highlighted)
            howToButton.titleLabel!.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
            howToButton.titleLabel!.textAlignment = .right
            howToButton.alpha = 0.0
            howToButton.tag = (ratesOption == "Проценты по 395 ГК РФ") ? 0 : 1
            howToButton.addTarget(self, action: #selector(CalculatorController.howToPressed(_:)), for: .touchUpInside)
            howToButton.translatesAutoresizingMaskIntoConstraints = false
            courtStack.addArrangedSubview(howToButton)

            let dutyLabel = UILabel()
            dutyLabel.numberOfLines = 1
            dutyLabel.textColor = blueTheme["main"]!
            dutyLabel.font = UIFont.monospacedDigitSystemFont(ofSize: UIFont.systemFontSize * 3.25, weight: UIFont.Weight.regular)
            dutyLabel.text = "0,00"
            dutyLabel.translatesAutoresizingMaskIntoConstraints = false

            rateLabels.append(dutyLabel)
            howToButtons.append(howToButton)

            stackView.addArrangedSubview(courtStack)
            stackView.addArrangedSubview(dutyLabel)
            scrollDisplay.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: scrollDisplay, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))

            displays.append(stackView)
        }
        view.addSubview(scrollDisplay)

        keypad = UIView()
        keypad.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(keypad)

        cDisplay = CostsDisplay()
        cDisplay.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(cDisplay)

        cTitleLabel = UILabel()
        cTitleLabel.numberOfLines = 1
        cTitleLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 22, weight: UIFont.Weight.light)
        cTitleLabel.text = "0,00"
        cTitleLabel.textColor = UIColor.darkGray
        cTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        cDisplay.addSubview(cTitleLabel)

        let cDisplayLablesDictionary:[String: UIView] = ["titleLabel": cTitleLabel]

        let cCourtLabelH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-24-[titleLabel]-24-|", options: NSLayoutFormatOptions(), metrics: nil, views: cDisplayLablesDictionary)
        cDisplay.addConstraints(cCourtLabelH)

        let cLablesV = NSLayoutConstraint.constraints(withVisualFormat: "V:|-12-[titleLabel]-12-|", options: [], metrics: nil, views: cDisplayLablesDictionary)
        cDisplay.addConstraints(cLablesV)

        for button in cButtons {
            let cButton = UIButton()
            cButton.backgroundColor = UIColor.clear
            cButton.setTitleColor(UIColor.darkGray, for: .normal)
            cButton.setTitleColor(blueTheme["main"]!, for: .highlighted)
            cButton.setTitleColor(blueTheme["main"]!, for: .selected)
            cButton.translatesAutoresizingMaskIntoConstraints = false
            switch button {
            case "VAT":
                cButton.setTitle("1 февраля 2015", for: .normal)
                cButton.setTitleColor(.lightGray, for: .normal)
                cButton.setTitleColor(.darkGray, for: .highlighted)
                cButton.setTitleColor(blueTheme["main"]!, for: .selected)
                cButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
                cButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
                cButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 26.0, bottom: 0.0, right: 0.0)
                cButton.addTarget(self, action: #selector(CalculatorController.changeVAT(_:)), for: .touchUpInside)
                cButton.tag = 0
            case "CBR":
                cButton.setImage(UIImage(named: "rightChevron"), for: .disabled)
                cButton.isEnabled = false
            case "Days":
                cButton.setTitle("23 марта 2015", for: .normal)
                cButton.setTitleColor(.lightGray, for: .normal)
                cButton.setTitleColor(.darkGray, for: .highlighted)
                cButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
                cButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 26.0)
                cButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
                cButton.addTarget(self, action: #selector(CalculatorController.changeVAT(_:)), for: .touchUpInside)
                cButton.tag = 1
            default: break

            }
            keypad.addSubview(cButton)
            buttons.append(cButton)
        }

        pageControl = UIPageControl()
        pageControl.numberOfPages = displays.count
        pageControl.currentPageIndicatorTintColor = blueTheme["main"]!
        pageControl.pageIndicatorTintColor = blueTheme["lightGrey"]!
        pageControl.addTarget(self, action: #selector(self.whf), for: .valueChanged)
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageControl)

        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: (self.view.bounds.width / 3), height: ((self.view.bounds.height * 0.382) / 4))

        let keypadDisplay: UICollectionView! = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        keypadDisplay.backgroundColor = UIColor.clear
        keypadDisplay.dataSource = self
        keypadDisplay.delegate = self
        keypadDisplay.register(KeypadCell.self, forCellWithReuseIdentifier: "KeypadCell")
        keypadDisplay.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "backspaceCell")
        keypadDisplay.translatesAutoresizingMaskIntoConstraints = false
        
        keypadDisplay.delaysContentTouches = false
        view.addSubview(keypadDisplay)

        let kC = NSLayoutConstraint(item: keypadDisplay, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.height, multiplier: 0.382, constant: 1)
        view.addConstraint(kC)

        let viewsDictionary = ["topBar": topBar, "scrollDisplay": scrollDisplay, "pageControl": pageControl, "display395": displays[0], "display317": displays[1], "keypad": keypad, "cDisplay": cDisplay, "keypadDisplay": keypadDisplay]  as [String : Any]

        let courtDisplaysHorizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|[display395(==keypad)][display317(==display395)]|", options: [], metrics: nil, views: viewsDictionary)
        view.addConstraints(courtDisplaysHorizontal)

        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[cDisplay]|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))

        let displayH = NSLayoutConstraint.constraints(withVisualFormat: "H:|[scrollDisplay]|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary)
        view.addConstraints(displayH)

        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[topBar]|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))

        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[keypad]|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))

        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[keypadDisplay]|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))

        let buttonsD:[String: UIView] = ["buttonVAT": buttons[0], "buttonDays": buttons[1], "buttonCBR": buttons[2]]

        keypad.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[buttonVAT][buttonCBR(==12)][buttonDays(==buttonVAT)]|", options: .alignAllCenterY, metrics: nil, views: buttonsD))

        keypad.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[buttonVAT]|", options: [], metrics: nil, views: buttonsD))

        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[pageControl(==keypad)]|", options: [], metrics: nil, views: viewsDictionary))

        let viewsV = NSLayoutConstraint.constraints(withVisualFormat: "V:|-26-[topBar(>=32,<=54)]-[scrollDisplay][pageControl(==10)]-[cDisplay][keypad(==cDisplay)][keypadDisplay]-54-|", options: [], metrics: nil, views: viewsDictionary)
        view.addConstraints(viewsV)

        let copyTap = UITapGestureRecognizer(target: self, action: #selector(copyDutyLabel))
        copyTap.numberOfTapsRequired = 2
        scrollDisplay.addGestureRecognizer(copyTap)

        let pasteTap = UITapGestureRecognizer(target: self, action: #selector(pasteCostsLabel))
        pasteTap.numberOfTapsRequired = 2
        cDisplay.addGestureRecognizer(pasteTap)

        let swipeToDelete = UISwipeGestureRecognizer(target: self, action: #selector(clearLogic))
        swipeToDelete.direction = .left
        cDisplay.addGestureRecognizer(swipeToDelete)
    }

    @objc func pasteCostsLabel() {
        let cleanString = (UIPasteboard.general.string == nil) ? "0,00" : UIPasteboard.general.string!.stripAllDigits()
        
        if !cleanString.isEmpty {
            self.costDictionary = ["isTypping": true, "isCommaAvailable": false, "firstDecimal": true, "secondDecimal": true]
        }

        cTitleLabel.attributedText = StringHelpers.shared.changeColor(string: MathController().numberFormatter.string(from: NSNumber(floatLiteral: (cleanString as NSString).doubleValue))!, withDictionary: costDictionary)
        checkAndCount()
    }

    func setupUserPrefferences() {
        if UserDefaults.standard.object(forKey: "usersRegion") == nil {
            UserDefaults.standard.set(1, forKey: "usersRegion")
        }

        if UserDefaults.standard.object(forKey: "usersCalendar") == nil || UserDefaults.standard.object(forKey: "calendarFix") == nil {
            UserDefaults.standard.set(360, forKey: "usersCalendar")
            UserDefaults.standard.set(true, forKey: "calendarFix")
        }
        UserDefaults.standard.synchronize()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cButtons2.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if cButtons2[(indexPath as NSIndexPath).row] == "backspace" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "backspaceCell", for: indexPath)
            let image = UIImageView(image: UIImage(named: "backspace"))
            image.translatesAutoresizingMaskIntoConstraints = false

            cell.addSubview(image)

            cell.addConstraint(NSLayoutConstraint(item: image, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: cell, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))

            cell.addConstraint(NSLayoutConstraint(item: image, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: cell, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))

            let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(CalculatorController.clearLongPressed(_:)))
            longPressGesture.minimumPressDuration = 0.25
            cell.addGestureRecognizer(longPressGesture)

            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KeypadCell", for: indexPath) as! KeypadCell
            cell.textLabel.text = self.cButtons2[(indexPath as NSIndexPath).row]

            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath)! as? KeypadCell {
            self.numericAndCommaPressed(cell.textLabel.text!)
        } else {
            self.clearLogic()
        }
    }

    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath)! as? KeypadCell {
            cell.textLabel.textColor = blueTheme["main"]!
        }
    }

    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath)! as? KeypadCell {
            cell.textLabel.textColor = UIColor.darkGray
        }
    }

    @objc func howToPressed(_ sender: UIButton) {
        let stageCtrl = StageController(style: .grouped)
        stageCtrl.stages = (sender.tag == 0) ? self.stages : self.stages317
        stageCtrl.tag = sender.tag
        stageCtrl.totalSumm = cTitleLabel.text!.replacingOccurrences(of: ",", with: ".", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.caseInsensitive, range: nil).toDouble
        present(UINavigationController(rootViewController: stageCtrl), animated: true, completion: nil)
    }

    func numericAndCommaPressed(_ typpedCharacter: String) {
        let existingCharacter = cTitleLabel.text!
        let characterCounter = existingCharacter.split(separator: ",")[0].count

        if characterCounter < 15 {
            if costDictionary["secondDecimal"]! == false {
                showCosts(typpedCharacter, existingCharacter: existingCharacter, costLabel: cTitleLabel)
            }
        } else if characterCounter == 15 {
            if typpedCharacter == "," {
                showCosts(typpedCharacter, existingCharacter: existingCharacter, costLabel: cTitleLabel)
            } else {
                if costDictionary["isCommaAvailable"]! == false {
                    if costDictionary["secondDecimal"] == false {
                        showCosts(typpedCharacter, existingCharacter: existingCharacter, costLabel: cTitleLabel)
                    }
                }
            }
        }
    }

    func showCosts(_ typpedCharacter:String, existingCharacter:String, costLabel:UILabel) {
        let existingCharacter = existingCharacter.replacingOccurrences(of: " ", with: "")
        var combinedCharacters = String(stringInterpolationSegment: MathController().numberFormatter.number(from: existingCharacter)!) + typpedCharacter
        var doubleResult = MathController().numberFormatter.string(from: NSNumber(floatLiteral: (combinedCharacters as NSString).doubleValue))!

        if costDictionary["isTypping"] == true {
            if typpedCharacter == "," {
                if costDictionary["isCommaAvailable"] == true {
                    costDictionary["isCommaAvailable"] = false
                }
            } else {
                if costDictionary["isCommaAvailable"] == false {
                    if costDictionary["firstDecimal"] == false && costDictionary["secondDecimal"] == false {
                        let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 3)

                        combinedCharacters = String(existingCharacter[..<index]) + "." + typpedCharacter
                        costDictionary["firstDecimal"] = true
                        doubleResult = MathController().numberFormatter.string(from: NSNumber(floatLiteral: (combinedCharacters as NSString).doubleValue))!
                    } else if costDictionary["firstDecimal"] == true && costDictionary["secondDecimal"] == false {
                        let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 1)
                        combinedCharacters = String(existingCharacter[..<index]) + typpedCharacter
                        combinedCharacters = combinedCharacters.replacingOccurrences(of: ",", with: ".")
                        costDictionary["secondDecimal"] = true
                        doubleResult = MathController().numberFormatter.string(from: NSNumber(floatLiteral: (combinedCharacters as NSString).doubleValue))!
                    }
                }
            }
        } else {
            costDictionary["isTypping"] = true
            if typpedCharacter == "," {
                costDictionary["isCommaAvailable"] = false
            }
        }

        costLabel.attributedText = StringHelpers.shared.changeColor(string: doubleResult, withDictionary: costDictionary)

        checkAndCount()
    }

    @objc func changeCalendar(_ sender: AnyObject!) {
        if UserDefaults.standard.integer(forKey: "usersCalendar") == 360 {
            calendarAnimation(365)
        } else {
            calendarAnimation(360)
        }
    }

    func calendarAnimation(_ calendarStyle: Int) {
        UserDefaults.standard.set(calendarStyle, forKey: "usersCalendar")
        UserDefaults.standard.synchronize()
        let title = (calendarStyle == 360) ? "360" : "365/366"
        self.calendarBtn.setTitle(title, for: .normal)
        self.checkAndCount()
    }

    @objc func changeVAT(_ sender: AnyObject) {
        self.daysAlertView = DaysAlertView()
        self.daysAlertView.mode = (sender.tag == 0) ? .startDate : .endDate

        self.daysAlertView.todayDate = (self.buttons[sender.tag].isSelected) ? self.buttons[sender.tag].titleLabel?.text!.toDate : Date()

        self.daysAlertView.show({ (selectedDate) -> Void in
            let theNewDate:[Int] = selectedDate as! Array
            var morningOfChristmasComponents = DateComponents()
            morningOfChristmasComponents.year = theNewDate[2]
            morningOfChristmasComponents.month = theNewDate[1]
            morningOfChristmasComponents.day = theNewDate[0]

            var chekingCompononets = DateComponents()

            chekingCompononets.year = theNewDate[2]
            chekingCompononets.month = theNewDate[1]
            chekingCompononets.day = 1

            let checkingDate = Calendar.current.date(from: chekingCompononets)

            let cal = Calendar.current
            let flags: NSCalendar.Unit = [.year, .month, .day]
            let lastDayOfMonth = checkingDate?.endOfMonth()

            let checked = (cal as NSCalendar).components(flags, from: lastDayOfMonth!)

            if morningOfChristmasComponents.day! > checked.day! {
                morningOfChristmasComponents.day = checked.day
            }

            let morningOfChristmas = Calendar.current.date(from: morningOfChristmasComponents)!
            self.buttons[sender.tag].setTitle(morningOfChristmas.dateFormatter.string(from: morningOfChristmas), for: .normal)
            self.buttons[sender.tag].isSelected = true
            self.checkAndCount()
        })
    }

    @objc func regionPickerPressed(_ sender: AnyObject) {
        self.regionAlertView = RegionAlertView()

        self.regionAlertView.show({ (selectedRegion) -> Void in
            let newRegion = (selectedRegion as! Int)
            if newRegion != UserDefaults.standard.integer(forKey: "usersRegion") {
                UserDefaults.standard.set(newRegion, forKey: "usersRegion")
                UserDefaults.standard.synchronize()
                self.regionPicker.setTitle(self.availableRegions[newRegion], for: .normal)
                self.countRates()
            }
        })
    }

    func checkAndCount() {
        if self.buttons[0].isSelected && self.buttons[1].isSelected {
            if self.cTitleLabel.text?.toNumber != 0.0 {
                for button in self.howToButtons {
                    button.alpha = 1.0
                }
            } else {
                for button in self.howToButtons {
                    button.alpha = 0.0
                }
            }
            self.countRates()
        }
    }

    @objc func copyDutyLabel() {
        UIPasteboard.general.string = rateLabels[self.pageControl.currentPage].text!

        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.copyLabel.alpha = 1.0
        }, completion: nil)

        UIView.animate(withDuration: 1.0, delay: 1.5, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.copyLabel.alpha = 0.0
        }, completion: nil)
    }


    @objc func clearLongPressed(_ sender:UILongPressGestureRecognizer) {
        if sender.state == .began {
            timer = Timer.scheduledTimer(timeInterval: 0.35, target: self, selector: #selector(CalculatorController.clearLogic), userInfo: nil, repeats: true)
        } else if sender.state == .ended {
            timer.invalidate()
        }
    }

    @objc func clearLogic() {
        let existingCharacter = cTitleLabel.text!.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ",", with: ".").replacingOccurrences(of: " ", with: "")

        var clearedCharactres:String! = "0,00"

        if costDictionary["isTypping"] == true {
            if costDictionary["isCommaAvailable"] == true {
                let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 3)
                var result = String(existingCharacter[..<index])
                result.removeLast()

                clearedCharactres = result
            } else {
                if costDictionary["firstDecimal"] == false && costDictionary["secondDecimal"] == false {
                    costDictionary["isCommaAvailable"] = true
                    let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 3)
                    clearedCharactres = String(existingCharacter[..<index])
                }
                if costDictionary["firstDecimal"] == true && costDictionary["secondDecimal"] == false {
                    let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 2)
                    clearedCharactres = String(existingCharacter[..<index])

                    costDictionary["firstDecimal"] = false
                } else if costDictionary["firstDecimal"] == true && costDictionary["secondDecimal"] == true {
                    let index: String.Index = existingCharacter.index(existingCharacter.startIndex, offsetBy: existingCharacter.count - 1)
                    clearedCharactres = String(existingCharacter[..<index])

                    costDictionary["secondDecimal"] = false
                }
            }
        }

        if (clearedCharactres as NSString).doubleValue.isZero && costDictionary["isCommaAvailable"] == true {
            costDictionary["isTypping"] = false
        }

        cTitleLabel.attributedText = StringHelpers.shared.changeColor(string: costsCurrencyFormatter.string(from: NSNumber(floatLiteral: clearedCharactres.toDouble))!, withDictionary: costDictionary)
        checkAndCount()
    }

    func countRates() {
        let sDate = buttons[0].titleLabel?.text!.toDate
        let eDate = buttons[1].titleLabel?.text!.toDate
        var datesArray: [[Date]] = []

        let costs = cTitleLabel.text!.toNumber

        if UserDefaults.standard.integer(forKey: "usersCalendar") == 365 {
            let calendar = Calendar.current
            let sDateYear = calendar.dateComponents([.year], from: sDate!).year
            let eDateYear = calendar.dateComponents([.year], from: eDate!).year

            let diff: Int = abs(eDateYear! - sDateYear!)
            
            for index in 0..<diff+1 {
                var first: Date?
                var second: Date?

                let beginYear = calendar.date(from: DateComponents(year: sDateYear! + index))
                let endYear = calendar.date(from: DateComponents(year: sDateYear! + index + 1))?.addingTimeInterval(-1)

                    first = getMin(first: beginYear, second: sDate)
                    second = getMax(first: endYear, second: eDate)

                if !datesArray.isEmpty {
                    if let lastArray = datesArray.last, let startDate = lastArray.first {
                        if let startDateYear = calendar.dateComponents([.year], from: startDate).year {
                            if !checkIfLeapYear(year: startDateYear) {
                                datesArray.removeLast()
                                
                                first = startDate
                            }
                        }
                    }
                }

                if let first = first, let second = second {
                    datesArray.append([first, second])
                }
            }
        } else {
            if let startDate = sDate, let endDate = eDate {
                datesArray.append([startDate, endDate])
            }
        }
        
        newFunc(costs: costs, dates: datesArray)
        newFunc317(costs: costs, dates: datesArray)
    }
    
    func getMin(first: Date?, second: Date?) -> Date? {
        if let first = first, let second = second {
            let array: [Date] = [first, second].sorted(by: {$0.timeIntervalSince1970 > $1.timeIntervalSince1970})
            if let result = array.first {
                return result
            }
        }
        
        return nil
    }
    
    func getMax(first: Date?, second: Date?) -> Date? {
        if let first = first, let second = second {
            let array: [Date] = [first, second].sorted(by: {$0.timeIntervalSince1970 < $1.timeIntervalSince1970})
            if let result = array.first {
                return result
            }
        }
        
        return nil
    }

    func newFunc(costs: NSNumber, dates: [[Date]]) {
        self.stages = []

        // Дни
        var countedDays: Int = 0
        var resultedDays: Int = 0
        // Сумма
        var countedPenalty: Double = 0.0
        var resultedPenalty: Double = 0.0
        let region = UserDefaults.standard.integer(forKey: "usersRegion")

        var penaltyArray:[Double] = []

        var startDate4Stage: Date!
        var endDate4Stage: Date!
        var rateDay: Double = 0.0

        for date in dates {
            let sDate = date[0]
            let eDate = date[1]
            var yearDays: Int {
                if UserDefaults.standard.integer(forKey: "usersCalendar") == 365 {
                    if let year = Calendar(identifier: .gregorian).dateComponents([.year], from: sDate).year {
                        return checkIfLeapYear(year: year) ? 366 : 365
                    }
                }
                
                return UserDefaults.standard.integer(forKey: "usersCalendar")
            }

            let filtered:[Rate] = rates.filter({ rate in rate.endDate >= sDate && rate.startDate <= eDate})

            for rate in filtered {
                let rightRate = rate.rate[region].isZero ? 0 : region
                if rate.startDate <= sDate && rate.endDate >= sDate && rate.endDate <= eDate {
                    countedDays = countDays(fromDate: sDate, toDate: rate.endDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                    penaltyArray = countRate(costs, cbr: rate.rate[rightRate], days: countedDays, yearDays: yearDays)

                    startDate4Stage = sDate
                    endDate4Stage = rate.endDate
                } else if rate.startDate >= sDate && rate.endDate >= sDate && rate.endDate <= eDate {
                    if rate.startDate >= sDate && rate.endDate <= eDate {
                        countedDays = countDays(fromDate: rate.startDate, toDate: rate.endDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                        penaltyArray = countRate(costs, cbr: rate.rate[rightRate], days: countedDays, yearDays: yearDays)

                        startDate4Stage = rate.startDate as Date
                        endDate4Stage = rate.endDate as Date
                    } else if rate.startDate >= sDate && rate.endDate >= eDate  {
                        countedDays = countDays(fromDate: rate.startDate, toDate: eDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                        penaltyArray = countRate(costs, cbr: rate.rate[rightRate], days: countedDays, yearDays: yearDays)

                        startDate4Stage = rate.startDate as Date
                        endDate4Stage = eDate as Date
                    }
                } else if rate.startDate >= sDate && rate.endDate >= eDate {
                    countedDays = countDays(fromDate: rate.startDate, toDate: eDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                    penaltyArray = countRate(costs, cbr: rate.rate[rightRate], days: countedDays, yearDays: yearDays)

                    startDate4Stage = rate.startDate as Date
                    endDate4Stage = eDate as Date
                } else if rate.startDate <= sDate && rate.endDate >= eDate {
                    countedDays = countDays(fromDate: sDate, toDate: eDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                    penaltyArray = countRate(costs, cbr: rate.rate[rightRate], days: countedDays, yearDays: yearDays)

                    startDate4Stage = sDate
                    endDate4Stage = eDate
                }

                countedPenalty = penaltyArray[1]
                rateDay = penaltyArray[0]

                resultedDays += countedDays

                let stage = RateStage(rateYear: rate.rate[rightRate], rateDay: rateDay, daysTotal: countedDays, rateTotal: Double(countedPenalty.toString.replacingOccurrences(of: ",", with: ".", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil))!, startDate: startDate4Stage, endDate: endDate4Stage, yearDays: yearDays)
                self.stages.append(stage)
            }
        }

        for stage in self.stages {
            resultedPenalty += stage.rateTotal
        }

        rateLabels[0].text = resultedPenalty.toString
    }

    func newFunc317(costs: NSNumber, dates: [[Date]]) {
        self.stages317 = []

        // Дни
        var countedDays: Int = 0
        var resultedDays: Int = 0
        // Сумма
        var countedPenalty: Double = 0.0
        var resultedPenalty: Double = 0.0

        var penaltyArray:[Double] = []

        var startDate4Stage: Date!
        var endDate4Stage: Date!
        var rateDay: Double = 0.0

        for date in dates {
            let sDate = date[0]
            let eDate = date[1]
            
            var yearDays: Int {
                if UserDefaults.standard.integer(forKey: "usersCalendar") == 365 {
                    if let year = Calendar(identifier: .gregorian).dateComponents([.year], from: sDate).year {
                        return checkIfLeapYear(year: year) ? 366 : 365
                    }
                }
                
                return UserDefaults.standard.integer(forKey: "usersCalendar")
            }

            let filtered:[Rate] = rates317.filter({ rate in rate.endDate >= sDate && rate.startDate <= eDate})

            for rate in filtered {
                let rightRate = rate.rate[0]
                if rate.startDate <= sDate && rate.endDate >= sDate && rate.endDate <= eDate {
                    countedDays = countDays(fromDate: sDate, toDate: rate.endDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                    penaltyArray = countRate(costs, cbr: rightRate, days: countedDays, yearDays: yearDays)

                    startDate4Stage = sDate
                    endDate4Stage = rate.endDate
                } else if rate.startDate >= sDate && rate.endDate >= sDate && rate.endDate <= eDate {
                    if rate.startDate >= sDate && rate.endDate <= eDate {
                        countedDays = countDays(fromDate: rate.startDate, toDate: rate.endDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                        penaltyArray = countRate(costs, cbr: rightRate, days: countedDays, yearDays: yearDays)

                        startDate4Stage = rate.startDate as Date
                        endDate4Stage = rate.endDate as Date
                    } else if rate.startDate >= sDate && rate.endDate >= eDate  {
                        countedDays = countDays(fromDate: rate.startDate, toDate: eDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                        penaltyArray = countRate(costs, cbr: rightRate, days: countedDays, yearDays: yearDays)

                        startDate4Stage = rate.startDate as Date
                        endDate4Stage = eDate as Date
                    }
                } else if rate.startDate >= sDate && rate.endDate >= eDate {
                    countedDays = countDays(fromDate: rate.startDate, toDate: eDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                    penaltyArray = countRate(costs, cbr: rightRate, days: countedDays, yearDays: yearDays)

                    startDate4Stage = rate.startDate as Date
                    endDate4Stage = eDate as Date
                } else if rate.startDate <= sDate && rate.endDate >= eDate {
                    countedDays = countDays(fromDate: sDate, toDate: eDate, daysInMonth: UserDefaults.standard.integer(forKey: "usersCalendar"))
                    penaltyArray = countRate(costs, cbr: rightRate, days: countedDays, yearDays: yearDays)

                    startDate4Stage = sDate
                    endDate4Stage = eDate
                }

                countedPenalty = penaltyArray[1]
                rateDay = penaltyArray[0]

                resultedDays += countedDays

                let stage = RateStage(rateYear: rightRate, rateDay: rateDay, daysTotal: countedDays, rateTotal: Double(countedPenalty.toString.replacingOccurrences(of: ",", with: ".", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil))!, startDate: startDate4Stage, endDate: endDate4Stage, yearDays: yearDays)
                self.stages317.append(stage)
            }
        }

        for stage in self.stages317 {
            resultedPenalty += stage.rateTotal
        }

        rateLabels[1].text = resultedPenalty.toString
    }

    func trim(_ string:String) -> String {
        let fRegexp = try? NSRegularExpression(pattern: "(?!\\.|,)(\\D)", options: .caseInsensitive)
        let sRegexp = try? NSRegularExpression(pattern: "(^[\\.]{1,100})|([\\.]{1,100}$)", options: .caseInsensitive)

        let a = fRegexp?.stringByReplacingMatches(in: string, options: [], range: NSMakeRange(0, string.count), withTemplate: "").replacingOccurrences(of: ",", with: ".")
        let b = sRegexp?.stringByReplacingMatches(in: a!, options: [], range: NSMakeRange(0, (a!).count), withTemplate: "")

        return b!
    }

    func trimB(_ string:String, deleteAll:Bool) -> String {
        var fRegexp:NSRegularExpression
        var n:String
        if deleteAll {
            fRegexp = try! NSRegularExpression(pattern: "(,\\d{2})", options: .caseInsensitive)
            n = fRegexp.stringByReplacingMatches(in: string, options: [], range: NSMakeRange(0, string.count), withTemplate: "")
        } else {
            fRegexp = try! NSRegularExpression(pattern: "(.0{2})", options: .caseInsensitive)
            n = fRegexp.stringByReplacingMatches(in: string, options: [], range: NSMakeRange(0, string.count), withTemplate: "")
        }

        return n
    }

    func replaceDotByComma(_ string: String) -> String {
        let result = string.replacingOccurrences(of: ".", with: ",", options: NSString.CompareOptions.literal, range: nil)

        return result
    }

    func countDays(fromDate: Date, toDate: Date, daysInMonth: Int) -> Int {
        let cal = Calendar.current
        var count: Int
        
        if daysInMonth == 360 {
            let flags: Set<Calendar.Component> = [.year, .month, .day]

            let components = cal.dateComponents(flags, from: fromDate, to: toDate)
            print(components)
            count = 1 + (360 * components.year!) + (30 * components.month!) + (components.day!)

            if components.month! == 0 {
                let endMonth = cal.dateComponents(flags, from: toDate.endOfMonth()!)
                
                if (28 >= endMonth.day! && endMonth.day! <= 31) && count >= 28 {
                    count = 30
                }
            }
            
            return count
        } else {
            count = cal.dateComponents([.day], from: fromDate, to: toDate).day! + 1
        }
        return count
    }

    func checkIfLeapYear(year: Int) -> Bool {
        if year % 4 == 0 {
            if year % 100 == 0 {
                if year % 400 == 0 {
                    return true
                } else {
                    return false
                }
            } else {
                return true
            }
        } else {
            return false
        }
    }

    func countRate(_ costsOfAction: NSNumber, cbr: Double, days: Int, yearDays: Int) -> [Double] {
        let yearDays = (UserDefaults.standard.integer(forKey: "usersCalendar") == 360) ? 360 : yearDays
        let oneDayCbr = cbr / (100.0 * Double(yearDays))
        let calculatedDuty = costsOfAction.doubleValue * (oneDayCbr * Double(days))
        
        return [oneDayCbr, calculatedDuty]
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage:Int = NSInteger(scrollDisplay.contentOffset.x / scrollDisplay.frame.size.width)
        self.pageControl.currentPage = currentPage
    }
    
    func scrollToPage(_ currentPage:Int, animated: Bool) {
        let goToPage: CGRect = scrollDisplay!.frame
        
        self.scrollDisplay!.setContentOffset(CGPoint(x: (goToPage.size.width * CGFloat(currentPage)), y: 0), animated: animated)
        self.pageControl.currentPage = currentPage
    }
    
    @objc func whf() {
        scrollToPage(self.pageControl.currentPage, animated: true)
    }
}

