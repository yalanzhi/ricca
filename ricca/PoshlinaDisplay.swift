//
//  CostsDisplay.swift
//  nitti
//
//  Created by Andrei Ialanzhi on 17.08.16.
//  Copyright © 2016 Andrei Ialanzhi. All rights reserved.
//

import UIKit

class PoshlinaDisplay: UIView {
    override init (frame : CGRect) {
        super.init(frame : frame)
        addBehavior()
    }

    convenience init () {
        self.init(frame:CGRect.zero)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }

    func addBehavior (){
        self.backgroundColor = blueTheme["lightGrey"]!

        var spacersArray:[UIView] = []

        for index in 0..<4 {
            let spacerLight = UIView()
            if index == 1 || index == 2 {
                spacerLight.backgroundColor = UIColor(white: 246.0/255.0, alpha: 1.0)
            } else {
                spacerLight.backgroundColor = UIColor(white: 245.0/255.0, alpha: 1.0)
            }

            spacerLight.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(spacerLight)

            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[spacerLight]|", options: [], metrics: nil, views: ["spacerLight": spacerLight]))

            spacersArray.append(spacerLight)
        }

        let plusSign = UIButton()
        plusSign.setImage(UIImage(named: "plus"), for: .normal)
        plusSign.addTarget(CalculatorController(), action: #selector(PoshlinaController().stateChanged(_:)), for: .touchUpInside)
        plusSign.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(plusSign)

        let costsDictionary = ["spacerTopLight": spacersArray[0], "spacerBottomLight": spacersArray[1], "spacerTopDark": spacersArray[2], "spacerBottomDark": spacersArray[3], "plusSign": plusSign]

        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[plusSign(20)]-18.5-|", options: [], metrics: nil, views: costsDictionary))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[plusSign(20)]", options: [], metrics: nil, views: costsDictionary))

        self.addConstraint(NSLayoutConstraint(item: plusSign, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))

        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[spacerTopDark(0.5)][spacerTopLight(0.5)]", options: [], metrics: nil, views: costsDictionary))

        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[spacerBottomLight(0.5)][spacerBottomDark(0.5)]|", options: [], metrics: nil, views: costsDictionary))
    }
}
