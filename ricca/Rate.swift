//
//  Rate.swift
//  karma395
//
//  Created by Andrei Ialanzhi on 27.07.15.
//  Copyright (c) 2015 Andrei Ialanzhi. All rights reserved.
//

import Foundation

struct Rate {
    let rate: [Double]
    let startDate: Date
    let endDate: Date
}

struct RateStage {
    let rateYear:  Double
    let rateDay:   Double
    let daysTotal: Int
    let rateTotal: Double
    let startDate: Date
    let endDate:   Date
    let yearDays:   Int
}

let rates: [Rate] = [
    Rate(rate: [8.25,8.25,8.25,8.25,8.25,8.25,8.25,8.25,8.25], startDate:"14 09 2012".toDate, endDate:"31 05 2015".toDate),
    Rate(rate: [11.80, 11.44, 11.24, 10.46, 11.15, 11.27, 10.89, 11.20, 14.18], startDate:"01 06 2015".toDate, endDate:"14 06 2015".toDate),
    Rate(rate: [11.70, 11.37, 11.19, 10.70, 11.16, 11.14, 10.81, 11.18, 13.31], startDate:"15 06 2015".toDate, endDate:"14 07 2015".toDate),
    Rate(rate: [10.74, 10.36, 10.25, 9.64, 10.14, 10.12, 9.89, 10.40, 9.89], startDate:"15 07 2015".toDate, endDate:"16 08 2015".toDate),
    Rate(rate: [10.51, 10.11, 10.14, 9.49, 10.12, 9.96, 9.75, 10.00, 9.07], startDate:"17 08 2015".toDate, endDate:"14 09 2015".toDate),
    Rate(rate: [9.91, 9.55, 9.52, 9.00, 9.59, 9.50, 9.21, 9.71, 8.53], startDate:"15 09 2015".toDate, endDate:"14 10 2015".toDate),
    Rate(rate: [9.49, 9.29, 9.17, 8.72, 9.24, 9.09, 9.02, 9.46, 8.17], startDate:"15 10 2015".toDate, endDate:"16 11 2015".toDate),
    Rate(rate: [9.39, 9.25, 9.11, 8.73, 9.15, 9.20, 9.00, 9.26, 7.75], startDate:"17 11 2015".toDate, endDate:"14 12 2015".toDate),
    Rate(rate: [7.32, 7.08, 6.93, 6.34, 7.07, 7.44, 7.18, 7.64, 8.09], startDate:"15 12 2015".toDate, endDate:"24 01 2016".toDate),
    Rate(rate: [7.94, 7.72, 7.53, 7.01, 7.57, 7.89, 7.81, 8.06, 8.32], startDate:"25 01 2016".toDate, endDate:"18 02 2016".toDate),
    Rate(rate: [8.96, 8.72, 8.73, 8.23, 8.69, 8.57, 9.00, 8.69, 8.82], startDate:"19 02 2016".toDate, endDate:"16 03 2016".toDate),
    Rate(rate: [8.64, 8.41, 8.45, 7.98, 8.29, 8.44, 8.81, 8.60, 8.76], startDate:"17 03 2016".toDate, endDate:"14 04 2016".toDate),
    Rate(rate: [8.14, 7.85, 7.77, 7.32, 7.76, 7.92, 8.01, 8.01, 8.37], startDate:"15 04 2016".toDate, endDate:"18 05 2016".toDate),
    Rate(rate: [7.90, 7.58, 7.45, 7.05, 7.53, 7.74, 7.71, 7.62, 8.12], startDate:"19 05 2016".toDate, endDate:"15 06 2016".toDate),
    Rate(rate: [8.24, 7.86, 7.81, 7.40, 7.82, 7.89, 7.93, 7.99, 8.20], startDate:"16 06 2016".toDate, endDate:"14 07 2016".toDate),
    Rate(rate: [7.52, 7.11, 7.01, 6.66, 7.10, 7.15, 7.22, 7.43, 8.19], startDate: "15 07 2016".toDate, endDate: "31 07 2016".toDate),
    Rate(rate: [10.50,10.50,10.50,10.50,10.50,10.50,10.50,10.50,10.50], startDate: "01 08 2016".toDate, endDate: "18 09 2016".toDate),
    Rate(rate: [10.00,10.00,10.00,10.00,10.00,10.00,10.00,10.00,10.00], startDate: "19 09 2016".toDate, endDate: "26 03 2017".toDate),
    Rate(rate: [9.75,9.75,9.75,9.75,9.75,9.75,9.75,9.75,9.75], startDate: "27 03 2017".toDate, endDate: "01 05 2017".toDate),
    Rate(rate: [9.25,9.25,9.25,9.25,9.25,9.25,9.25,9.25,9.25], startDate: "02 05 2017".toDate, endDate: "18 06 2017".toDate),
    Rate(rate: [9.00,9.00,9.00,9.00,9.00,9.00,9.00,9.00,9.00], startDate: "19 06 2017".toDate, endDate: "17 09 2017".toDate),
    Rate(rate: [8.50,8.50,8.50,8.50,8.50,8.50,8.50,8.50,8.50], startDate: "18 09 2017".toDate, endDate: "29 10 2017".toDate),
    Rate(rate: [8.25,8.25,8.25,8.25,8.25,8.25,8.25,8.25,8.25], startDate: "30 10 2017".toDate, endDate: "17 12 2017".toDate),
    Rate(rate: [7.75,7.75,7.75,7.75,7.75,7.75,7.75,7.75,7.75], startDate: "18 12 2017".toDate, endDate: "11 02 2018".toDate),
    Rate(rate: [7.50,7.50,7.50,7.50,7.50,7.50,7.50,7.50,7.50], startDate: "12 02 2018".toDate, endDate: "25 03 2018".toDate),
    Rate(rate: [7.25,7.25,7.25,7.25,7.25,7.25,7.25,7.25,7.25], startDate: "26 03 2018".toDate, endDate: "18 09 2021".toDate)
]

let rates317: [Rate] = [
    Rate(rate: [8.25], startDate: "14 09 2012".toDate, endDate: "31 12 2015".toDate),
    Rate(rate: [11.0], startDate: "01 01 2016".toDate, endDate: "13 06 2016".toDate),
    Rate(rate: [10.5], startDate: "14 06 2016".toDate, endDate: "18 09 2016".toDate),
    Rate(rate: [10.0], startDate: "19 09 2016".toDate, endDate: "26 03 2017".toDate),
    Rate(rate: [9.75], startDate: "27 03 2017".toDate, endDate: "01 05 2017".toDate),
    Rate(rate: [9.25], startDate: "02 05 2017".toDate, endDate: "18 06 2017".toDate),
    Rate(rate: [9.00], startDate: "19 06 2017".toDate, endDate: "17 09 2017".toDate),
    Rate(rate: [8.50], startDate: "18 09 2017".toDate, endDate: "29 10 2017".toDate),
    Rate(rate: [8.25], startDate: "30 10 2017".toDate, endDate: "17 12 2017".toDate),
    Rate(rate: [7.75], startDate: "18 12 2017".toDate, endDate: "11 02 2018".toDate),
    Rate(rate: [7.50], startDate: "12 02 2018".toDate, endDate: "25 03 2018".toDate),
    Rate(rate: [7.25], startDate: "26 03 2018".toDate, endDate: "18 09 2021".toDate)
]
