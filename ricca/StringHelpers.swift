//
//  StringHelpers.swift
//  ricca
//
//  Created by Andrei Ialanzhi on 26.02.17.
//  Copyright © 2017 Andrei Ialanzhi. All rights reserved.
//

import UIKit

class StringHelpers {
    static var shared = StringHelpers()
    
    func changeColor(string:String, withDictionary: [String: Bool]) -> NSMutableAttributedString {
        var minus:Int = 0
        if withDictionary["isTypping"] == true {
            if withDictionary["isCommaAvailable"] == true {
                minus = 3
            } else {
                if withDictionary["firstDecimal"] == false && withDictionary["secondDecimal"] == false {
                    minus = 2
                } else if withDictionary["firstDecimal"] == true && withDictionary["secondDecimal"] == false {
                    minus = 1
                } else {
                    minus = 0
                }
            }
        } else {
            minus = 4
        }

        let mutableString = NSMutableAttributedString(string: string)
        mutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: blueTheme["main"]!, range: NSMakeRange(0, string.count - minus))
        
        return mutableString
    }
}
