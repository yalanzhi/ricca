//
//  KeypadCell.swift
//  nitti
//
//  Created by Andrew Yalanzhi on 12.08.16.
//  Copyright (c) 2016 Andrei Ialanzhi. All rights reserved.
//

import UIKit

class KeypadCell: UICollectionViewCell {
    var textLabel: UILabel! = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        keypadCellStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func keypadCellStyle() {
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.textAlignment = .center
        textLabel.textColor = .darkGray
        textLabel.font = UIFont.monospacedDigitSystemFont(ofSize: 22, weight: UIFont.Weight.light)

        self.contentView.addSubview(textLabel)
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[textLabel]|", options: [], metrics: nil, views: ["textLabel": textLabel]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[textLabel]|", options: [], metrics: nil, views: ["textLabel": textLabel]))
    }
}
