//
//  ClaimShowController.swift
//  pandorum
//
//  Created by Andrew Yalanzhi on 22.03.15.
//  Copyright (c) 2015 Andrei Ialanzhi. All rights reserved.
//

import UIKit

class StageController: UITableViewController {

    var tag: Int! = 0

    var stages:[RateStage] = []
    var totalResult: Double = 0.0
    var totalSumm: Double = 0.0

    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateStyle = DateFormatter.Style.long
        formatter.dateFormat = "dd.MM.yyyy"

        return formatter
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        let titleTag = tag == 0 ? "395" : "317.1"
        self.title = "\(titleTag): результат"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(StageController.dismiss(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareResult))
        tableView.tableFooterView = footerView()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 46.5))
        headerView.backgroundColor = UIColor.white

        let datePeriod = UILabel()
        datePeriod.textColor = blueTheme["main"]
        datePeriod.text = "\(dateFormatter.string(from: self.stages[section].startDate as Date)) – \(dateFormatter.string(from: self.stages[section].endDate as Date))"
        datePeriod.font = UIFont.monospacedDigitSystemFont(ofSize: 17, weight: UIFont.Weight.bold)
        datePeriod.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(datePeriod)

        let separator = UIView()
        separator.backgroundColor = blueTheme["lightGrey"]
        separator.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(separator)

        let headerDictionary = ["datePeriod":datePeriod, "separator": separator]
        let dPh = NSLayoutConstraint.constraints(withVisualFormat: "H:|-19-[datePeriod]-19-|", options: [], metrics: nil, views: headerDictionary)
        headerView.addConstraints(dPh)

        let sH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-19-[separator]-19-|", options: [], metrics: nil, views: headerDictionary)
        headerView.addConstraints(sH)

        let dPv = NSLayoutConstraint.constraints(withVisualFormat: "V:[datePeriod]-9-[separator(1)]|", options: NSLayoutFormatOptions.alignAllLeading, metrics: nil, views: headerDictionary)
        headerView.addConstraints(dPv)

        return headerView
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "StageCell")
        let stage = self.stages[(indexPath as NSIndexPath).section]

        let separatorTop = UIView()
        separatorTop.translatesAutoresizingMaskIntoConstraints = false
        cell.contentView.addSubview(separatorTop)

        let separatorBottom = UIView()
        separatorBottom.translatesAutoresizingMaskIntoConstraints = false
        cell.contentView.addSubview(separatorBottom)

        let label = UILabel()
        label.textColor = blueTheme["grey"]
        switch (indexPath as NSIndexPath).row {
        case 0:
            label.text = "Количество дней"
        case 1:
            label.text = "Количество дней в году"
        case 2:
            label.text = "Процентная ставка"
        default:
            break
        }
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        cell.contentView.addSubview(label)

        let total = UILabel()
        total.textColor = blueTheme["grey"]
        switch (indexPath as NSIndexPath).row {
        case 0:
            total.text = String(stage.daysTotal)
        case 1:
            total.text = "\(stage.yearDays)"
        case 2:
            total.text = stage.rateYear.toString
        default:
            break
        }
        total.textAlignment = .right
        total.font = UIFont.monospacedDigitSystemFont(ofSize: 13, weight: UIFont.Weight.bold)
        
        total.translatesAutoresizingMaskIntoConstraints = false
        cell.contentView.addSubview(total)

        let sTh = NSLayoutConstraint.constraints(withVisualFormat: "H:|[separatorTop]|", options: [], metrics: nil, views: ["separatorTop": separatorTop])
        cell.contentView.addConstraints(sTh)

        let sTv = NSLayoutConstraint(item: separatorTop, attribute: .height, relatedBy: NSLayoutRelation.equal, toItem: label, attribute: .height, multiplier: 0, constant: 0)
        cell.contentView.addConstraint(sTv)

        let sBh = NSLayoutConstraint.constraints(withVisualFormat: "H:|[separatorBottom]|", options: [], metrics: nil, views: ["separatorBottom": separatorBottom])
        cell.contentView.addConstraints(sBh)

        let sBv = NSLayoutConstraint(item: separatorBottom, attribute: .height, relatedBy: NSLayoutRelation.equal, toItem: separatorBottom, attribute: .height, multiplier: 0, constant: 0)
        cell.contentView.addConstraint(sBv)

        let dPh = NSLayoutConstraint.constraints(withVisualFormat: "H:|-19-[label(>=210,<=236)][total(>=30@751)]-19-|", options: NSLayoutFormatOptions.alignAllTop, metrics: nil, views: ["label":label, "total": total])
        cell.contentView.addConstraints(dPh)

        let dPv = NSLayoutConstraint.constraints(withVisualFormat: "V:|-9-[sT][label][sB]-9-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["label":label, "total": total, "sT": separatorTop, "sB": separatorBottom])
        cell.contentView.addConstraints(dPv)

        sTv.constant = 12

        return cell
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.white

        let separator = UIView()
        separator.backgroundColor = UIColor(white: 245.0/255.0, alpha: 1.0)
        separator.translatesAutoresizingMaskIntoConstraints = false
        footerView.addSubview(separator)

        let totalLabel = UILabel()
        totalLabel.textColor = blueTheme["grey"]
        totalLabel.text = "Промежуточный итог"
        totalLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        totalLabel.translatesAutoresizingMaskIntoConstraints = false
        footerView.addSubview(totalLabel)
        
        let result = self.stages[section].rateTotal.toString

        let rateTotal = UILabel()
        rateTotal.textColor = blueTheme["main"]
        rateTotal.text = result
        rateTotal.textAlignment = .right
        rateTotal.font = UIFont.monospacedDigitSystemFont(ofSize: 13, weight: UIFont.Weight.bold)
        rateTotal.translatesAutoresizingMaskIntoConstraints = false
        footerView.addSubview(rateTotal)

        let footerDictionary = ["totalLabel":totalLabel, "rateTotal": rateTotal, "separator": separator]

        let sH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-19-[separator]-19-|", options: [], metrics: nil, views: footerDictionary)
        footerView.addConstraints(sH)

        let dPh = NSLayoutConstraint.constraints(withVisualFormat: "H:|-19-[totalLabel(==160)][rateTotal(>=70)]-19-|", options: NSLayoutFormatOptions.alignAllTop, metrics: nil, views: footerDictionary)
        footerView.addConstraints(dPh)

        let dPv = NSLayoutConstraint.constraints(withVisualFormat: "V:|[separator(0.5)]-16-[totalLabel]", options: NSLayoutFormatOptions(), metrics: nil, views: footerDictionary)
        footerView.addConstraints(dPv)

        return footerView
    }

    func footerView() -> UIView? {
        let footerView = UIView()
        footerView.frame.size.height = 97
        footerView.backgroundColor = UIColor.white

        let separator = UIView()
        separator.backgroundColor = UIColor(white: 245.0/255.0, alpha: 1.0)
        separator.translatesAutoresizingMaskIntoConstraints = false
        footerView.addSubview(separator)

        let totalLabel = UILabel()
        totalLabel.textColor = blueTheme["grey"]
        totalLabel.text = "Итого"
        totalLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        totalLabel.translatesAutoresizingMaskIntoConstraints = false
        footerView.addSubview(totalLabel)

        let rateTotal = UILabel()
        rateTotal.textColor = blueTheme["main"]

        for stage in self.stages {
            totalResult +=  Double(stage.rateTotal.toString.replacingOccurrences(of: ",", with: ".", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil))!
        }
        
        rateTotal.text = totalResult.toString
            
        rateTotal.textAlignment = .right
        rateTotal.font = UIFont.monospacedDigitSystemFont(ofSize: 13, weight: UIFont.Weight.bold)
        rateTotal.translatesAutoresizingMaskIntoConstraints = false
        footerView.addSubview(rateTotal)
        
        let footerDictionary = ["totalLabel":totalLabel, "rateTotal": rateTotal, "separator": separator]

        let sH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-19-[separator]-19-|", options: [], metrics: nil, views: footerDictionary)
        footerView.addConstraints(sH)

        let dPh = NSLayoutConstraint.constraints(withVisualFormat: "H:|-19-[totalLabel(==160)][rateTotal(>=70)]-19-|", options: NSLayoutFormatOptions.alignAllTop, metrics: nil, views: footerDictionary)
        footerView.addConstraints(dPh)

        let dPv = NSLayoutConstraint.constraints(withVisualFormat: "V:|[separator(1)]-20-[totalLabel]", options: NSLayoutFormatOptions(), metrics: nil, views: footerDictionary)
        footerView.addConstraints(dPv)
        
        return footerView
    }
    
    @objc func shareResult(sender: UIButton) {
        var textToShare = "\(self.title!)\n---\n\n"
        textToShare += "Общая сумма задолженности: \(totalSumm.toString) руб.\n"
        if self.tag == 0 {
            textToShare += "Место жительства (нахождения) должника: \(RegionAlertView().pickerRegions[UserDefaults.standard.integer(forKey: "usersRegion")])\n\n------\n\n"
        }
        
        for stage in self.stages {
            textToShare += "\(dateFormatter.string(from: stage.startDate)) – \(dateFormatter.string(from: stage.endDate))\n---\n"
            textToShare += "Количество дней: \(stage.daysTotal)\n"
            textToShare += "Процентная ставка: \(stage.rateYear.toString)%\n"
            textToShare += "Промежуточный итог: \(stage.rateTotal.toString) руб.\n\n------\n\n"
        }
        
        textToShare += "Итого: \(totalResult.toString) руб."
        
        let activityVC = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = sender
        self.present(activityVC, animated: true, completion: nil)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.stages.count
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 58
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 64
    }

    @objc func dismiss(_ sender: AnyObject!) {
        self.dismiss(animated: true, completion: {
            self.stages = [] })
    }

    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}
