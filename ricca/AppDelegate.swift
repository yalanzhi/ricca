//
//  AppDelegate.swift
//  karma395
//
//  Created by Andrew Yalanzhi on 10.04.15.
//  Copyright (c) 2015 Andrei Ialanzhi. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.backgroundColor = .white
        UITabBar.appearance().tintColor = blueTheme["main"]!
        UITabBar.appearance().backgroundColor = blueTheme["lightGrey"]!
        UITabBar.appearance().isOpaque = false

        UINavigationBar.appearance().tintColor = blueTheme["main"]!

        UITableView.appearance().backgroundColor = .white
        let initController = CalculatorController()
        initController.tabBarItem = UITabBarItem(title: "Пени", image: UIImage(named: "PeniLogo"), tag: 1)

        let nittiController = PoshlinaController()
        nittiController.tabBarItem = UITabBarItem(title: "Пошлина", image: UIImage(named: "PoshlinaLogo"), tag: 1)

        let tabCtrl: UITabBarController = UITabBarController()
        tabCtrl.viewControllers = [initController, nittiController]

        window!.rootViewController = tabCtrl
        window!.makeKeyAndVisible()
        
        Fabric.with([Crashlytics.self])
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        UserDefaults.standard.synchronize() 
    }
}
