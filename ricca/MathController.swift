//
//  MathController.swift
//  nitti
//
//  Created by Andrei Ialanzhi on 03.05.16.
//  Copyright © 2016 Andrei Ialanzhi. All rights reserved.
//

import UIKit

class MathController {
    
    var numberFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.roundingMode = .halfEven
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.maximumIntegerDigits = 12
        return formatter
    }
    
    func countDuties(_ costsOfAction:String) -> [String] {
        var costsArray:[String] = []
        let arbitralDutyPressets:[Bool] = [false, true]
        let courtDutyPressets:[[Bool]] = [[false, false], [true, false], [true, true]]
        let costsOfAction = self.numberFormatter.number(from: costsOfAction)!.doubleValue
        let roundingIncrement = UserDefaults.standard.bool(forKey: "prefferedStyle")
        
        for arbitralDutyPresset in arbitralDutyPressets {
            costsArray.append(self.calculateArbitraryDutyFromCostOfAction(costsOfAction, discount: arbitralDutyPresset, roundingIncrement: roundingIncrement))
        }

        for courtDutyPresset in courtDutyPressets {
            costsArray.append(self.calculateCourtDutyFromCostOfAction(costsOfAction, discount: courtDutyPresset[0], consumer: courtDutyPresset[1], roundingIncrement: roundingIncrement))
        }

        return costsArray
    }
    
    func countSurchargeDuties(_ firstCostsOfAction:String, secondCostsOfAction:String) -> [String] {
        let firstCostsArray = self.countDuties(firstCostsOfAction)
        let secondCostsArray = self.countDuties(secondCostsOfAction)
        
        var surchargeArray:[String] = []
        
        for duty in 0..<firstCostsArray.count {
            let result:Double = (numberFormatter.number(from: firstCostsArray[duty])!.doubleValue - numberFormatter.number(from: secondCostsArray[duty])!.doubleValue)

            surchargeArray.append(numberFormatter.string(from: NSNumber(floatLiteral: result))!)
        }
        
        return surchargeArray
    }

    func calculateArbitraryDutyFromCostOfAction(_ costsOfAction:Double, discount:Bool, roundingIncrement:Bool) -> String {
        var preCalculatedDuty:Double = 0.0
        var calculatedDuty:Double = 0.0
        var finalDuty:Double = 0.0
        var stringDuty: String!

        if costsOfAction.isZero {
            calculatedDuty = 0.0
        } else if costsOfAction >= 0.01 && costsOfAction <= 100_000.00 {
            preCalculatedDuty = costsOfAction * 0.04
            
            calculatedDuty = (preCalculatedDuty <= 2_000.00) ? 2_000.00 : preCalculatedDuty
            
        } else if costsOfAction >= 100_000.01 && costsOfAction <= 200_000.00 {
            calculatedDuty = ((costsOfAction - 100_000.00) * 0.03) + 4_000
        } else if costsOfAction >= 200_000.01 && costsOfAction <= 1_000_000.00 {
            calculatedDuty = ((costsOfAction - 200_000.00) * 0.02) + 7_000
        } else if costsOfAction >= 1_000_000.01 && costsOfAction <= 2_000_000.00 {
            calculatedDuty = ((costsOfAction - 1_000_000.00) * 0.01) + 23_000
        } else {
            preCalculatedDuty = ((costsOfAction - 2_000_000.00) * 0.005) + 33_000
            
            calculatedDuty = (preCalculatedDuty > 200_000.00) ? 200_000.00 : preCalculatedDuty
        }
        
        finalDuty = (discount) ? (calculatedDuty / 2) : calculatedDuty
        finalDuty = roundingIncrement ? round(finalDuty) : finalDuty
        
        stringDuty = self.numberFormatter.string(from: NSNumber(floatLiteral: finalDuty))
        
        return stringDuty
    }
    
    func calculateCourtDutyFromCostOfAction(_ costsOfAction:Double, discount:Bool, consumer:Bool, roundingIncrement:Bool) -> String {
        var preCalculatedDuty:Double = 0.0
        var calculatedDuty:Double = 0.0
        var finalDuty:Double = 0.0
        var stringDuty: String!
        
        if costsOfAction.isZero {
            calculatedDuty = 0.0
        } else if costsOfAction >= 0.01 && costsOfAction <= 20_000.00 {
            preCalculatedDuty = costsOfAction * 0.04
            
            calculatedDuty = (preCalculatedDuty < 400.00) ? 400.00 : preCalculatedDuty
            
        } else if costsOfAction >= 20_000.01 && costsOfAction <= 100_000.00 {
            calculatedDuty = ((costsOfAction - 20_000.00) * 0.03) + 800
        } else if costsOfAction >= 100_000.01 && costsOfAction <= 200_000.00 {
            calculatedDuty = ((costsOfAction - 100_000.00) * 0.02) + 3_200
        } else if costsOfAction >= 200_000.01 && costsOfAction <= 1_000_000.00 {
            calculatedDuty = ((costsOfAction - 200_000.00) * 0.01) + 5_200
        } else {
            preCalculatedDuty = ((costsOfAction - 1_000_000.00) * 0.005) + 13_200
            
            calculatedDuty = (preCalculatedDuty > 60_000.00) ? 60_000.00 : preCalculatedDuty
        }
        
        if consumer {
            if costsOfAction >= 1_000_000.01 {
                preCalculatedDuty = (((costsOfAction - 200_000.00) * 0.01) + 5_200) - calculatedDuty
                
                finalDuty = (preCalculatedDuty > 46_800.00) ? 46_800.00 : preCalculatedDuty
            }
        } else {
            finalDuty = (discount) ? (calculatedDuty / 2) : calculatedDuty
        }

        finalDuty = roundingIncrement ? round(finalDuty) : finalDuty
        
        stringDuty = self.numberFormatter.string(from: NSNumber(floatLiteral: finalDuty))
        
        return stringDuty
    }
}
