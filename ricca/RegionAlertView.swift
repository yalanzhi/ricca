//
//  AlertView.swift
//  karma395
//
//  Created by Andrew Yalanzhi on 20.03.15.
//  Copyright (c) 2015 Andrei Ialanzhi. All rights reserved.
//

import UIKit

class RegionAlertView: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    let window: UIWindow = UIApplication.shared.windows[0] 
    var onSelection: ((AnyObject) -> Void)?
    fileprivate var selectedPickerValueIndex:Int = UserDefaults.standard.integer(forKey: "usersRegion")
    var header: UILabel!
    var result: String!

    let pickerRegions = ["Центральный федеральный округ", "Северо-Западный федеральный округ", "Южный федеральный округ", "Северо-Кавказский федеральный округ", "Приволжский федеральный округ", "Уральский федеральный округ", "Сибирский федеральный округ", "Дальневосточный федеральный округ", "Крымский федеральный округ"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame = CGRect(x: 0, y: 0, width: window.frame.size.width, height: window.frame.size.height)

        let alert = UIView()
        alert.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.97)
        alert.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(alert)

        let viewH = NSLayoutConstraint.constraints(withVisualFormat: "H:|[alert]|", options: [], metrics: nil, views: ["alert": alert])
        self.view.addConstraints(viewH)

        let viewV = NSLayoutConstraint.constraints(withVisualFormat: "V:|[alert]|", options: [], metrics: nil, views: ["alert": alert])
        self.view.addConstraints(viewV)

        let whiteView = UIView()
        whiteView.backgroundColor = UIColor.white
        whiteView.layer.borderWidth = 0.5
        whiteView.layer.borderColor = UIColor(red: 155.0/255.0, green: 186.0/255.0, blue: 196.0/255.0, alpha: 1.0).cgColor
        whiteView.layer.cornerRadius = 10
        whiteView.layer.shadowColor = UIColor(red: 235.0/255.0, green: 242.0/255.0, blue: 245.0/255.0, alpha: 1).cgColor
        whiteView.layer.shadowOpacity = 1
        whiteView.layer.shadowRadius = 20
        whiteView.frame = CGRect(x: (((UIApplication.shared.windows[0] ).frame.size.width - 40)/2), y: (UIApplication.shared.windows[0] ).frame.size.height - 260, width: (UIApplication.shared.windows[0] ).frame.size.width - 40, height: 260)
        whiteView.center = (UIApplication.shared.windows[0] ).center
        self.view.addSubview(whiteView)

        header = UILabel()
        header.font = UIFont.boldSystemFont(ofSize: 13)
        header.textAlignment = .center
        header.textColor = UIColor.darkGray
        header.numberOfLines = 2
        header.text = "Место жительства (нахождения) кредитора"
        header.translatesAutoresizingMaskIntoConstraints = false
        whiteView.addSubview(header)

        let separatorTop = UIView()
        separatorTop.backgroundColor = UIColor(red: 155.0/255.0, green: 186.0/255.0, blue: 196.0/255.0, alpha: 1.0)
        separatorTop.translatesAutoresizingMaskIntoConstraints = false
        whiteView.addSubview(separatorTop)

        let pickerDate = UIPickerView()
        pickerDate.dataSource = self
        pickerDate.delegate = self
        pickerDate.translatesAutoresizingMaskIntoConstraints = false
        whiteView.addSubview(pickerDate)

        let separatorBottom = UIView()
        separatorBottom.backgroundColor = UIColor(red: 155.0/255.0, green: 186.0/255.0, blue: 196.0/255.0, alpha: 1.0)
        separatorBottom.translatesAutoresizingMaskIntoConstraints = false
        whiteView.addSubview(separatorBottom)

        let separatorButtonsV = UIView()
        separatorButtonsV.backgroundColor = UIColor(red: 155.0/255.0, green: 186.0/255.0, blue: 196.0/255.0, alpha: 1.0)
        separatorButtonsV.translatesAutoresizingMaskIntoConstraints = false
        whiteView.addSubview(separatorButtonsV)

        let okButton = UIButton()
        okButton.setTitle("OK", for: .normal)
        okButton.setTitleColor(blueTheme["main"]!, for: .normal)
        okButton.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
        okButton.addTarget(self, action: #selector(RegionAlertView.save), for: .touchUpInside)
        okButton.translatesAutoresizingMaskIntoConstraints = false
        whiteView.addSubview(okButton)

        let cancelButton = UIButton()
        cancelButton.setTitle("Отмена", for: .normal)
        cancelButton.setTitleColor(.lightGray, for: .normal)
        cancelButton.setTitleColor(.darkGray, for: .highlighted)
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        cancelButton.addTarget(self, action: #selector(RegionAlertView.closed), for: .touchUpInside)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        whiteView.addSubview(cancelButton)

        let titleH = NSLayoutConstraint.constraints(withVisualFormat: "H:|[title]|", options: [], metrics: nil, views: ["title": header])
        whiteView.addConstraints(titleH)

        let separatorViewH = NSLayoutConstraint.constraints(withVisualFormat: "H:|[separatorTop]|", options: [], metrics: nil, views: ["separatorTop": separatorTop])
        whiteView.addConstraints(separatorViewH)

        let separatorBottomH = NSLayoutConstraint.constraints(withVisualFormat: "H:|[separatorBottom]|", options: [], metrics: nil, views: ["separatorBottom": separatorBottom])
        whiteView.addConstraints(separatorBottomH)

        let pickerDateH = NSLayoutConstraint.constraints(withVisualFormat: "H:|[pickerDate]|", options: [], metrics: nil, views: ["pickerDate": pickerDate])
        whiteView.addConstraints(pickerDateH)

        let buttonsH = NSLayoutConstraint.constraints(withVisualFormat: "H:|[cancelButton][separatorButtonsV(0.5)][okButton(==cancelButton)]|", options: NSLayoutFormatOptions.alignAllBottom, metrics: nil, views: ["okButton": okButton, "cancelButton": cancelButton, "separatorButtonsV": separatorButtonsV])
        whiteView.addConstraints(buttonsH)

        let separatorButtonsVV = NSLayoutConstraint.constraints(withVisualFormat: "V:[separatorButtonsV(48)]", options: NSLayoutFormatOptions(), metrics: nil, views: ["separatorButtonsV": separatorButtonsV])
        whiteView.addConstraints(separatorButtonsVV)

        let okButtonV = NSLayoutConstraint.constraints(withVisualFormat: "V:[okButton(48)]", options: NSLayoutFormatOptions(), metrics: nil, views: ["okButton": okButton])
        whiteView.addConstraints(okButtonV)

        let titleV = NSLayoutConstraint.constraints(withVisualFormat: "V:|-28-[title]-18-[separatorTop(0.5)]-[pickerDate]-[separatorBottom(0.5)][cancelButton(48)]|", options: [], metrics: nil, views: ["title": header, "separatorTop": separatorTop, "pickerDate": pickerDate, "separatorBottom": separatorBottom, "cancelButton": cancelButton])
        whiteView.addConstraints(titleV)

        pickerDate.selectRow(self.selectedPickerValueIndex, inComponent: 0, animated: true)
    }

    func show (_ selection: ((AnyObject) -> Void)?) {
        self.onSelection = selection

        self.view.alpha = 0.0
        window.addSubview(self.view)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.alpha = 1.0;
        })
    }

    @objc func save () {
        if self.onSelection != nil {
            self.onSelection!(self.selectedPickerValueIndex as AnyObject)
        }
        self.closed()
    }

    @objc func closed () {
        UIView.animate(withDuration: 0.3,
            animations: { () -> Void in
                self.view.alpha = 0.0;
            }, completion: { (completed) -> Void in
                self.view.removeFromSuperview()
        }) 
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerRegions.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerRegions[row]
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 48.0
    }

    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return pickerView.bounds.size.width
    }


    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLaber = UILabel()

        pickerLaber.text = pickerRegions[row]
        pickerLaber.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize, weight: UIFont.Weight.regular)

        pickerLaber.textAlignment = .center
        return pickerLaber
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedPickerValueIndex = row
    }
}
