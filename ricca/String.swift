//
//  StringExtensions.swift
//  Karma
//
//  Created by Andrei Ialanzhi on 15.07.15.
//  Copyright (c) 2015 Andrei Ialanzhi. All rights reserved.
//

import Foundation

extension String {
    var toDouble: Double {
        return (self as NSString).doubleValue
    }

    var numberFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2

        return formatter
    }

    var toNumber: NSNumber {
        return numberFormatter.number(from: self)!
    }

    var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "d MM yyyy"

        return dateFormatter
    }
    
    var toDate: Date {
        return dateFormatter.date(from: self)!
    }
}

extension Date {
    func countDaysFrom(_ startDate: Date!) -> Int {
        return Calendar.current.dateComponents([.day], from: startDate).day!
    }

    var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "d MMMM yyyy"

        return dateFormatter
    }
}

extension Double {
    var numberFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2

        return formatter
    }

    var toString: String {
        numberFormatter.roundingMode = .halfEven
        return numberFormatter.string(from: NSNumber(floatLiteral: self))!
    }
    
    var toStringHalfUp: String {
        numberFormatter.roundingMode = .halfEven
        return numberFormatter.string(from: NSNumber(floatLiteral: self))!
    }
}

extension Date {
    func endOfMonth(index: Int = 0) -> Date? {
        return Calendar.current.date(byAdding: DateComponents(month: (1 + index)), to: startOfMonth(self)!)!
    }
    
    func startOfMonth(_ date: Date) -> Date? {
        let calendar = Calendar.current
        return calendar.date(from: calendar.dateComponents([.year, .month], from: date))
    }
}

func >(l: Date, r: Date) -> Bool {
    return l.compare(r) == ComparisonResult.orderedDescending
}
func >=(l: Date, r: Date) -> Bool {
    return l.compare(r) == ComparisonResult.orderedDescending || l == r
}
func <(l: Date, r: Date) -> Bool {
    return l.compare(r) == ComparisonResult.orderedAscending
}
func <=(l: Date, r: Date) -> Bool {
    return l.compare(r) == ComparisonResult.orderedAscending || l == r
}
